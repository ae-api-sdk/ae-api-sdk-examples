///--------- author @kay-n ---------///


///------- ModelTiter_doble_1x1 string_1 ------///
///-----------      Position       ------------///

var width = Math.round(thisLayer.sourceRectAtTime().width);
var padding = 20;
var hideX = -width - padding;
var showX = padding;
var posY = transform.position[1];
var timeIn = comp("ModelScene_1x1")
    .layer(thisComp.name)
    .inPoint;
var timeOut = comp("ModelScene_1x1")
    .layer(thisComp.name)
    .outPoint;
var animationTime = 0.5;
var delay = 0.3;

if (time <= animationTime) {
    easeOut(time,
        0,
        animationTime,
        [hideX, posY],
        [showX, posY]
    );
} else {
    easeOut(time,
        timeOut - timeIn - animationTime - delay,
        timeOut - timeIn - delay,
        [showX, posY],
        [hideX, posY]
    );
}


///------- ModelTiter_doble_1x1 string_2 ------///
///-----------      Position       ------------///

var width = Math.round(thisLayer.sourceRectAtTime().width);
var padding = 20;
var hideX = -width - padding;
var showX = padding;
var posY = transform.position[1];
var timeIn = comp("ModelScene_1x1")
    .layer(thisComp.name)
    .inPoint;
var timeOut = comp("ModelScene_1x1")
    .layer(thisComp.name)
    .outPoint;
var animationTime = 0.5;
var delay = 0.3;

if (time <= animationTime + delay) {
    easeOut(time,
        delay,
        animationTime + delay,
        [hideX, posY],
        [showX, posY]
    );
} else {
    easeOut(time,
        timeOut - timeIn  - animationTime,
        timeOut - timeIn,
        [showX, posY],
        [hideX, posY]
    );
}




///-----------ModelTiter_double_1x1------------///
///-----------     SourceText      ------------///

var refText = thisComp.layer("reference_text")
    .text.sourceText.split("\r");

var stringID = parseInt(thisLayer.name.split("_")[1]);

refText[stringID - 1]




///-----------ModelTiter_simple_1x1------------///
///-----------      Position       ------------///

var width = Math.round(thisLayer.sourceRectAtTime().width);
var padding = 20;
var hideX = -width - padding;
var showX = padding;
var posY = transform.position[1];
var timeIn = comp("ModelScene_1x1")
    .layer(thisComp.name)
    .inPoint;
var timeOut = comp("ModelScene_1x1")
    .layer(thisComp.name)
    .outPoint;
var animationTime = 0.5;
if (time <= animationTime) {
    easeOut(time,
        0,
        animationTime,
        [hideX, posY],
        [showX, posY]
    );
} else {
    easeOut(time,
        timeOut - animationTime - timeIn,
        timeOut - timeIn,
        [showX, posY],
        [hideX, posY]
    );
}
