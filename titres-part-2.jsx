///--------- author @kay-n ---------///

{
    (function init(){
        var win = (this instanceof Panel)
            ? this
            : new Window("palette", 'Extendscript Notepad');

        var editText = win.add(
            'edittext',
            [0, 0, 300, 300],
            'Введите текст титров',
            {multiline: true}
        );

        var btnRun = win.add(
            'button',
            undefined,
            'run'
        );
        btnRun.onClick = function () {
            try {
                createTitres(editText.text.getTitresData());
            } catch (err) {
                alert(err)
            }
        };

        win.layout.layout(true);
        win.onResizing =
            win.onResize = function () {
                this.layout.resize();
            };
        win.layout.resize();

        if(win instanceof Window) {
            win.show();
        }
    })();
    function createTitres(data) {
        var scenesData = getScenesData();  // Получаем данные о сценах

        for(var i = 0; i < scenesData.length; i++) {
            var scene = getScene(scenesData[i]);       // Получаем композицию сцены
            var startTime = 0;                         // Переменная для расстановки титров на таймлайне

            for (var j = 0; j < data.length; j++) {
                var titreName = 'titre-' + j + '-' + scenesData[i].type;  // имя титра
                var layer = scene.layers.byName(titreName);               // пытаемя наяти его на сцене


                if (!layer) {   // если не нашли, создаем
                    var modelName = 'ModelTitre_' + data[j].type + '_' + scenesData[i].type;
                    var item = getTitreComp(modelName);     // создаем дубликат макета титра

                    if (item) {     // если дубликат создан, редактируем его
                        item.name = titreName;
                        layer = scene.layers.add(item);                             // добавляем титр на сцену
                        var modelLayer = scenesData[i].layers.byName(modelName);    // находим макет титра на макете сцены
                        startTime = editLayer(layer, startTime, modelLayer);        // редактируем настройки слоя
                        changeExpression(                                           // меняем в Expression ссылки на макет сцены ссылками на саму сцену
                            item,
                            'ModelScene_' + scenesData[i].type,
                            'scene-' + scenesData[i].type
                        );
                        setText(item.layers.byName('reference_text'), data[j].text);    //  вставляем текст
                    }
                } else { // если слой уже создан, просто меняем текст
                    setText(layer.source.layers.byName('reference_text'), data[j].text);
                }
            }
        }
    }

    // метод меняет строку search на строку replacement
    // в Expression всех свойств групп Transform
    // всех слоев копозиции
    function changeExpression(comp, search, replacement) {
        for (var i = 1; i <= comp.numLayers; i++) {
            var layer = comp.layer(i);
            var propGroup = layer.property('ADBE Transform Group');

            for (var j = 1; j <= propGroup.numProperties; j++) {
                var prop = propGroup.property(j);
                if (prop.expression) {
                    prop.expression = prop.expression.replace(
                        new RegExp(search, 'g'),
                        replacement
                    )
                }
            }
        }
    }

    // метод редактирует настройки слоя
    function editLayer(layer, startTime, modelLayer) {
        layer.startTime = startTime;

        if (modelLayer) {
            layer.label = modelLayer.label;
            var layerDuration = modelLayer.outPoint - modelLayer.startTime;
            layer.outPoint = startTime + layerDuration;
        } else {
            layer.outPoint = startTime + 5;
        }

        return layer.outPoint;
    }

    // метод меняет текст в текстовом слой
    function setText(layer, text) {
        if (layer) {
            var property = layer.text.property("Source Text");
            var value = property.value;
            value.text = text;
            property.setValue(value);
        }
    }

    // метод возвращает дубликат композиции
    function getTitreComp(modelName) {
        var item = getItem(modelName, CompItem);

        if (!item) {
            alert('Отсутствует модель ' + modelName);
            return null;
        }

        return item.duplicate();
    }

    // Метод, либо находит сцену в композиции,
    // либо создает новую, если ее еще нет
    function getScene(data) {
        var sceneName = 'scene-' + data.type;
        return getItem(sceneName, CompItem) ||
            app.project.items.addComp(
                sceneName,
                data.width,
                data.height,
                1,
                data.duration,
                data.frameRate
            );
    }

    // Метод находит элемент в проекте по имени,
    // или по имени и типу элемента
    function getItem(name, type) {
        var doc = app.project;

        for (var i = 1; i <= doc.numItems; i++) {
            if (doc.item(i).name === name) {
                if (type) {
                    if (doc.item(i) instanceof type) {
                        return doc.item(i);
                    }
                } else {
                    return doc.item(i);
                }
            }
        }
        return null;
    }

    // Метод находит все макеты сцен в проекте
    // и формирует из них массив с данными сцен
    function getScenesData() {
        var doc = app.project;
        var data = [];
        for (var i = 1; i <= doc.numItems; i++) {
            var item = doc.item(i);

            if (item instanceof CompItem &&
                /^ModelScene/.test(item.name)) {
                data.push({
                    type: item.name.split('_')[1],
                    width: item.width,
                    height: item.height,
                    frameRate: item.frameRate,
                    duration: item.duration,
                    layers: item.layers
                });
            }
        }
        return data;
    }
}

// Расширяем класс String, добавляя ему метод getTitresData
String.prototype.getTitresData = function() {
    return this
        .replace(/(^\n|^ |^"|\n+$| +$|"+$)/g, "")   // Удаляем из текста артефакты
        .split("\n\n")                              // Разбиваем текст на массив
        .map(function(d) {
            var str = d.replace(/(^\n|^ |\n+$| +$)/g, "")
            var tag = str.split(" ")[0];
            var text = str.replace(tag + ' ', '');
            return {                                // Формируем объекты для каждого титра
                type: tag.replace('#', ''),
                text: text,
            }
        });
}

// Расширяем класс Array, добавляя ему метод map
Array.prototype.map = function(callback) {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        arr.push(callback(this[i]));
    }
    return arr;
}