///--------- author @kay-n ---------///

{
    // Создаем окно интерфейса если оно не является таковым
    var win = (this instanceof Panel)
        ? this
        : new Window("palette", 'Extendscript Notepad');

    // Добавляем текстовое поле
    var editText = win.add(
        'edittext',                     // Тип добавляемого элемента
        [0, 0, 300, 300],               // Параметры верстки элемента [x, y, width, height]
        'alert("Hello, World!!!")',     // Текст по умолчанию
        { multiline: true }             // Указываем что текстовое поле многострочное
    );

    // Добавляем кнопку запуска кода
    var btnRun = win.add(
        'button',                       // Тип добавляемого элемента
        undefined,                      // Параметры верстки не указываем
        'run'                           // Текст на кнопке
    );
    // Обрабатываем событие нажатие кнопки
    btnRun.onClick = function() {
        try {
            eval(editText.text);        // Пробуем запустить скрипт из текстового поля
        } catch (e) {
            alert(e);                   // В случае неудачи
        }
    };

    // Делаем размер окна адаптивным
    win.layout.layout(true);
    win.onResizing  =
    win.onResize    = function () {
        this.layout.resize();
    };
    win.layout.resize();

    // Если тип окна Window, отображаем его.
    if(win instanceof Window) {
        win.show();
    }
}