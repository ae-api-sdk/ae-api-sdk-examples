///--------- author @kay-n ---------///

{
    var language = 'ru';
    var Text = {
        'ru': {
            'effects-list': {
                'panel-name': 'Выбор эффекта',
                'image-tunnel': 'Туннель картинок'
            },
            'apply-btn': 'Применить еффект',
            'errors': {
                'no-active-image': 'Выделите слой с изображением для применени эффекта',
                'no-image-layer': 'Выделите слой с изображением расширения jpg или png'
            },
            'utils': {

            }
        }
    };
    var Effects = [{
        name: 'image-tunnel',
        apply: function() {
            var scene = app.project.activeItem;
            if (!scene) {
                alert(Text[language].errors['no-active-image']);
                return;
            }
            var image = scene.selectedLayers[0];
            if (!image) {
                alert(Text[language].errors['no-active-image']);
                return;
            }
            if (!/.(jpg|png)/i.test(image.source.file)) {
                alert(Text[language].errors['no-image-layer']);
                return;
            }

            var camera = scene.layers.addCamera('camera', [scene.width * 0.5, scene.height * 0.5]);
            var cameraPosProp = camera.property('ADBE Transform Group').property('ADBE Position');
            cameraPosProp.setValue([scene.width * 0.5, scene.height * 0.5, cameraPosProp.value[2]])

            var controlLayer = scene.layers.addNull();
            controlLayer.moveAfter(camera);
            controlLayer.name = 'control';
            var distanceSlider = controlLayer.effect.addProperty("ADBE Slider Control");
            distanceSlider.name = 'distance';
            distanceSlider.property('Slider').setValue(100);
            var viewSlider  = controlLayer.effect.addProperty("ADBE Slider Control");
            viewSlider.name = 'field_of_view';
            viewSlider.property('Slider').setValue(10);

            scene.layers.precompose([image.index], 'ImageComp')
            var imageCompLayer = scene.layers.byName('ImageComp');
            imageCompLayer.threeDLayer = true;
            imageCompLayer.name = 'ImageComp-1';
            imageCompLayer.property('ADBE Transform Group').property('ADBE Position').expression = '[transform.position[0], transform.position[1], index * thisComp.layer("control").effect("distance")("Slider")]'

            var extractEffect = imageCompLayer.effect.addProperty('ADBE Extract');
            extractEffect.property('Black Point').expression = 'thisComp.layer("control").effect("field_of_view")("Slider") * (index - 1)';
            extractEffect.property('White Point').expression = 'thisComp.layer("control").effect("field_of_view")("Slider") * index';

            for (var i = 0; i < 30; i++) {
                var layer = imageCompLayer.duplicate();
                layer.name = 'ImageComp-' + (i + 2);
                layer.moveAfter(scene.layers.byName('ImageComp-' + (i + 1)))
            }
        }
    }];
    var Interface = {
        getEffectsList: function(group) {
            var panel = group.add("panel", [0,0,100,100], Text[language]['effects-list']['panel-name']);
            var list = panel.add('dropdownlist');
            list.itemSize = [150,20];
            for(var i = 0; i < Effects.length; i++) {
                var item = list.add('item', Text[language]['effects-list'][Effects[i].name]);
                item.name = Effects[i].name;
            }
            list.selection = 0;
            panel.currentEffect = list.selection.name;

            list.onChange = function(){
                panel.currentEffect = this.selection.name;
            };

            return panel;
        },
        getApplyBtn: function (group) {
            var btn = group.add('button', undefined, Text[language]['apply-btn']);
            btn.onClick = function() {
                btn.onChange({type: 'apply-effect'})
            }
            return btn;
        }
    };
    var Utils = {

    };

    (function init(){
        var win = (this instanceof Panel)
            ? this
            : new Window("palette", 'Extendscript Notepad');

        var group = win.add('group');

        var effectsList = Interface.getEffectsList(group);
        var applyBtn = Interface.getApplyBtn(group);

        applyBtn.onChange = eventHandler;

        win.layout.layout(true);
        win.onResizing  =
        win.onResize    = function () {
                this.layout.resize();
            };
        win.layout.resize();

        if(win instanceof Window) {
            win.show();
        }


        function eventHandler(e) {
            switch (e.type) {
                case 'apply-effect':
                    var effect = Effects.filter(function (effect) {
                        return effect.name === effectsList.currentEffect
                    })[0];
                    if (effect && typeof effect.apply === 'function') {
                        app.beginUndoGroup(effect.name);
                        effect.apply();
                        app.endUndoGroup();
                    }
                    break;
            }
        }
    })();
}

Array.prototype.filter = function (filterMethod) {
    if (typeof filterMethod !== 'function') {
        return this.slice();
    }

    var arr = [];
    for (var i in this) {
        if (filterMethod(this[i], i)) {
            arr.push(this[i]);
        }
    }
    return arr;
}