///--------- author @kay-n ---------///

{
    (function init(){
        var win = (this instanceof Panel)
            ? this
            : new Window("palette", 'Extendscript Notepad');

        var editText = win.add(
            'edittext',
            [0, 0, 300, 300],
            'Введите текст титров',
            {multiline: true}
        );

        var btnRun = win.add(
            'button',
            undefined,
            'run'
        );
        btnRun.onClick = function () {
            try {
                createTitres(editText.text.getTitresData());
            } catch (err) {
                alert(err)
            }
        };

        win.layout.layout(true);
        win.onResizing =
            win.onResize = function () {
                this.layout.resize();
            };
        win.layout.resize();

        if(win instanceof Window) {
            win.show();
        }
    })();

    // Этот метод будет создавать титры
    function createTitres(data) {
        var scenesData = getScenesData();  // Получаем данные о сценах

        for(var i = 0; i < scenesData.length; i++) {
            var scene = getScene(scenesData[i]);       // Получаем композицию сцены
        }
    }

    // Метод, либо находит сцену в композиции,
    // либо создает новую, если ее еще нет
    function getScene(data) {
        var sceneName = 'scene-' + data.type;
        return getItem(sceneName, CompItem) ||
            app.project.items.addComp(
                sceneName,
                data.width,
                data.height,
                1,
                data.duration,
                data.frameRate
            );
    }

    // Метод находит элемент в проекте по имени,
    // или по имени и типу элемента
    function getItem(name, type) {
        var doc = app.project;

        for (var i = 1; i <= doc.numItems; i++) {
            if (doc.item(i).name === name) {
                if (type) {
                    if (doc.item(i) instanceof type) {
                        return doc.item(i);
                    }
                } else {
                    return doc.item(i);
                }
            }
        }
        return null;
    }

    // Метод находит все макеты сцен в проекте
    // и формирует из них массив с данными сцен
    function getScenesData() {
        var doc = app.project;
        var data = [];
        for (var i = 1; i <= doc.numItems; i++) {
            var item = doc.item(i);

            if (item instanceof CompItem &&
                /^ModelScene/.test(item.name)) {
                data.push({
                    type: item.name.split('_')[1],
                    width: item.width,
                    height: item.height,
                    frameRate: item.frameRate,
                    duration: item.duration
                });
            }
        }
        return data;
    }
}

// Расширяем класс String, добавляя ему метод getTitresData
String.prototype.getTitresData = function() {
    return this
        .replace(/(^\n|^ |^"|\n+$| +$|"+$)/g, "")   // Удаляем из текста артефакты
        .split("\n\n")                              // Разбиваем текст на массив
        .map(function(d) {
            var str = d.replace(/(^\n|^ |\n+$| +$)/g, "")
            var tag = str.split(" ")[0];
            var text = str.replace(tag + ' ', '');
            return {                                // Формируем объекты для каждого титра
                type: tag.replace('#', ''),
                text: text,
            }
        });
}

// Расширяем класс Array, добавляя ему метод map
Array.prototype.map = function(callback) {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        arr.push(callback(this[i]));
    }
    return arr;
}