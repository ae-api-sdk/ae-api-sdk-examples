{
    (function init(){

        var panelGlobal = this;

        var palette = panelGlobal instanceof Panel
            ? panelGlobal
            : new Window("palette", 'Keyframe Wizard');

        var edittext1 = palette.add('edittext', [0, 0, 300, 100], undefined, { multiline: true });
        edittext1.helpTip = 'enter keyframes in a way like "x-y-z=time1 x-y-z=time2 x-y-z=time3", where timeN - number of frames from current time or from time(N-1);\n"+-/*" - for values arithmetics;\n"P:","S:","R:","O:","A:" - for transform property group\n"E:XY", "M:XY" - for effects and masks properties, where X and Y are indeces of properties in a given propertyGroup';
        edittext1.alignment = ["fill","top"];

        var button1 = palette.add("button", undefined, "OK");

        palette.layout.layout(true);
        palette.layout.resize();
        palette.onResizing = palette.onResize = function () {
            this.layout.resize();
        }

        if (palette instanceof Window) {
            palette.show();
        }

        button1.onClick = function() {
            try {
                runScript(edittext1.text)
            } catch (error) {
                alert('RunScript Error:\n' + error);
            }
        }
    })();

    function runScript(_typeIn) {
        app.beginUndoGroup("ar");

        var typeIn = _typeIn.replace(/ +/g, ' ').replace(/(^ | +$)/g, "");
        var currentComp = app.project.activeItem;
        var selectedLayers = currentComp.selectedLayers;
        var operationFlag = typeIn.slice(0, 1);
        var data = extractData(typeIn.slice(1));

        if (/\\/.test(operationFlag)) {
            setValuesToKeyFrames(selectedLayers, data);
        } else if (/\=/.test(operationFlag)) {
            setValuesToProps(selectedLayers, data);
        } else if (/[\+\-\*\/]/.test(operationFlag)) {
            applyCalculate(selectedLayers, data, operationFlag);
        } else if (/t/.test(operationFlag)) {
            setNewTime(selectedLayers, data, currentComp);
        } else {
            applySpecialOperation(selectedLayers, typeIn.split(":"));
        }
    }

    function operRift(val, inData) {
        //now we should make it for values with different dimentions
        var operationFlag = inData.slice(0, 1)
        var data = inData.slice(1).split("-")

        if (data.length > 1) {
            var addVal = [];
            for(var i = 0; i < data.length; i++) {
                addVal.push(parseFloat(data[i]))
            }
            if (val instanceof Array) {
                if (/[\*\/]/.test(operationFlag)) {
                    alert("I can't do it :)!");
                } else if (/[\+\-]/.test(operationFlag)) {
                    return eval('val' + operationFlag + 'addVal');
                }
            } else {
                return eval('val' + operationFlag + 'addVal[0]');
            }
        } else if (data.length) {
            return eval('val' + operationFlag + 'parseFloat(data[0])');
        } else {
            new Error('После знака операции "' + operationFlag + '" не указано значение')
        }
    }

    function extractData(ourStr) {
        try {
            var currentComp = app.project.activeItem;
            var kFrs = ourStr.split(" ");
            var ourXs = [];
            var ourYs = [];
            var ourZs = [];
            var ourTimes = [];
            var ourTime = currentComp.time;
            for (var i = 0; i < kFrs.length; i++) {
                var ourVal = kFrs[i].split("=")[0];
                var X = ourVal.split("-")[0];
                var Y = ourVal.split("-")[1];
                var Z = ourVal.split("-")[2];
                ourTime += kFrs[i].split("=")[1] * currentComp.frameDuration;
                ourXs.push(X);
                ourYs.push(Y);
                ourZs.push(Z);
                ourTimes.push(ourTime);
            }
            return [ourXs, ourYs, ourZs, ourTimes];
        } catch (e) {
            return [];
        }
    }

    function rd_Scooter_shiftKeyToNewTime(prop, keyToCopy, offset, keyToRemove) {


        // Remember the key's settings before creating the new setting, just in case creating the new key affects keyToCopy's settings
        var inInterp = prop.keyInInterpolationType(keyToCopy);
        var outInterp = prop.keyOutInterpolationType(keyToCopy);
        var keyToCopyValue = prop.keyValue(keyToCopy);

//		$.writeln("shifting key#"+keyToCopy+" of prop '"+prop.name+"'");

        if ((inInterp === KeyframeInterpolationType.BEZIER) && (outInterp === KeyframeInterpolationType.BEZIER)) {
            var tempAutoBezier = prop.keyTemporalAutoBezier(keyToCopy);
            var tempContBezier = prop.keyTemporalContinuous(keyToCopy);
        }
        if (outInterp !== KeyframeInterpolationType.HOLD) {
            var inTempEase = prop.keyInTemporalEase(keyToCopy);
            var outTempEase = prop.keyOutTemporalEase(keyToCopy);
        }
        if ((prop.propertyValueType === PropertyValueType.TwoD_SPATIAL) || (prop.propertyValueType === PropertyValueType.ThreeD_SPATIAL)) {
            var spatAutoBezier = prop.keySpatialAutoBezier(keyToCopy);
            var spatContBezier = prop.keySpatialContinuous(keyToCopy);
            var inSpatTangent = prop.keyInSpatialTangent(keyToCopy);
            var outSpatTangent = prop.keyOutSpatialTangent(keyToCopy);
            var roving = prop.keyRoving(keyToCopy);
        }

        // Create the new keyframe
        var newTime = prop.keyTime(keyToCopy) + offset;
//		$.writeln("adding new key...");
        var newKeyIndex = prop.addKey(newTime);
//		$.writeln("setting new key's value...");
        prop.setValueAtKey(newKeyIndex, keyToCopyValue);

        if (outInterp !== KeyframeInterpolationType.HOLD) {
//			$.writeln("setting temp ease...");
            prop.setTemporalEaseAtKey(newKeyIndex, inTempEase, outTempEase);
        }

        // Copy over the keyframe settings
//		$.writeln("setting interp...");
        prop.setInterpolationTypeAtKey(newKeyIndex, inInterp, outInterp);

        if ((inInterp === KeyframeInterpolationType.BEZIER) && (outInterp === KeyframeInterpolationType.BEZIER) && tempContBezier) {
//			$.writeln("setting temp cont...");
            prop.setTemporalContinuousAtKey(newKeyIndex, tempContBezier);
//			$.writeln("setting temp auto bez...");
            prop.setTemporalAutoBezierAtKey(newKeyIndex, tempAutoBezier);		// Implies Continuous, so do after it
        }

        if ((prop.propertyValueType === PropertyValueType.TwoD_SPATIAL) || (prop.propertyValueType === PropertyValueType.ThreeD_SPATIAL)) {
//			$.writeln("setting spat cont...");
            prop.setSpatialContinuousAtKey(newKeyIndex, spatContBezier);
//			$.writeln("setting spat auto bez...");
            prop.setSpatialAutoBezierAtKey(newKeyIndex, spatAutoBezier);		// Implies Continuous, so do after it

//			$.writeln("setting spat tangents...");
            prop.setSpatialTangentsAtKey(newKeyIndex, inSpatTangent, outSpatTangent);

//			$.writeln("setting roving...");
            prop.setRovingAtKey(newKeyIndex, roving);
        }

        // Remove the old keyframe
//		$.writeln("removing key...");
        prop.removeKey(keyToRemove);
    }

    function setValuesToKeyFrames(selectedLayers, data) {
        var newX, newY, newZ;

        for (var layerIndex = 0; layerIndex < selectedLayers.length; layerIndex++) {
            var currentLayer = selectedLayers[layerIndex];
            if (currentLayer.selectedProperties.length === 0) {
                alert('На слое "' + currentLayer.name + '" не выделено ни одного свойства');
            } else {
                var layerProps = currentLayer.selectedProperties;
                for (var propIndex = 0; propIndex < layerProps.length; propIndex++) {
                    var currentProp = layerProps[propIndex];
                    if (layerProps[propIndex].numKeys > 0) {
                        var currentPropKeys = currentProp.selectedKeys;
                        for (var keyIndex = 0; keyIndex < currentPropKeys.length; keyIndex++) {
                            var currentPropValues = currentProp.keyValue(currentPropKeys[keyIndex]);
                            if (currentPropValues instanceof Array) {
                                newX = (data[0] ? data[0][keyIndex] : currentPropValues[0]) || 0;
                                newY = (data[1] ? data[1][keyIndex] : currentPropValues[1]) || 0;
                                newZ = (data[2] ? data[2][keyIndex] : currentPropValues[2]) || 0;

                                currentProp.setValueAtKey(currentProp.selectedKeys[keyIndex], [newX, newY, newZ]);
                            } else {
                                newX = data[0] ? data[0][keyIndex] : currentPropValues[0];
                                currentProp.setValueAtKey(currentProp.selectedKeys[keyIndex], newX);
                            }
                        }
                    } else {
                        alert('На слое "' + currentLayer.name + '" в свойстве "' + currentProp.name + '" не выделено ни одного кефрейма');
                    }
                }
            }
        }
    }

    function setValuesToProps(selectedLayers, data) {
        var allIndeces = [];
        for (var m = 0; m < selectedLayers.length; m++) {
            var ind = selectedLayers[m].index;
            allIndeces.push(ind);
        }
        var minInd = Math.min.apply(Math, allIndeces);

        for (var layerIndex = 0; layerIndex < selectedLayers.length; layerIndex++) {
            var currentProps = selectedLayers[layerIndex].selectedProperties;
            for (var propIndex = 0; propIndex < currentProps.length; propIndex++) {
                if (currentProps[propIndex].selectedKeys.length > 0) {
                    for (var keyIndex = 0; keyIndex < currentProps[propIndex].selectedKeys.length; keyIndex++) {
                        var ourKey = currentProps[propIndex].selectedKeys[keyIndex];
                        var kV = currentProps[propIndex].keyValue(ourKey);
                        var kT = currentProps[propIndex].keyTime(ourKey);

                        //apply our indeces
                        for (var z = 1; z <= selectedLayers[layerIndex].index - minInd; z++) {
                            var nV = operRift(kV, data);
                            kV = nV;
                        }
                        currentProps[propIndex].setValueAtTime(kT, kV);
                    }
                } else {
                    var currentValue = currentProps[propIndex].value;

                    for (var z = 0; z <= selectedLayers[layerIndex].index - minInd; z++) {
                        var nV = operRift(currentValue, data);
                        currentValue = nV;
                    }
                    currentProps[propIndex].setValue(currentValue);
                }
            }
        }
    }

    function applyCalculate(selectedLayers, data, operationFlag) {
        for (var i = 0; i < selectedLayers.length; i++) {
            var currentLayerProps = selectedLayers[i].selectedProperties;
            for (var k = 0; k < currentLayerProps.length; k++) {
                var ourVal = currentLayerProps[k].value;
                var currentProp = currentLayerProps[k];
                if (currentProp.selectedKeys.length > 0) {
                    for (var j = 0; j < currentProp.selectedKeys.length; j++) {
                        var currentKey = currentProp.selectedKeys[j];
                        var kV = currentProp.keyValue(currentKey);
                        var kT = currentProp.keyTime(currentKey);
                        var nV = operate(kV, data, operationFlag);
                        currentProp.setValueAtTime(kT, nV);
                    }
                } else {
                    var nV = operate(ourVal, data, operationFlag);
                    currentProp.setValue(nV);
                }
            }
        }
    }

    function operate(val, sourceData, operationFlag) {
        var data = sourceData.split("-");

        //now we should make it for values with different dimentions
        if (data.length > 1) {
            var addVal = [];
            for (var i = 0; i < data.length; i++) {
                addVal.push(parseFloat(data[i]))
            }

            if (val instanceof Array) {
                if(/[\*\/]/.test(operationFlag)) {
                    alert("I can't do it :)!");
                } else if(/[\+\-]/.test(operationFlag)) {
                    return eval('val' + operationFlag + 'addVal')
                }
            } else if(/[\*\/\-\+]/.test(operationFlag)) {
                return eval('val' + operationFlag + 'addVal[0]')
            }

        } else if (data.length) {
            return eval('val' + operationFlag + 'parseFloat(data)')
        }

        return 0;
    }

    function setNewTime(selectedLayers, data, currentComp) {
        for (var i = 0; i < selectedLayers.length; i++) {
            var myProps = selectedLayers[i].selectedProperties;
            for (var z = 0; z < myProps.length; z++) {
                var myKeys = myProps[z].selectedKeys;
                if (myKeys.length % 2 === 1) {
                    alert("Choose couples");
                } else {
                    var timePoint = myProps[z].keyTime(myKeys[0]);
                    var newTime = timePoint + data * currentComp.frameDuration;
                    var oldTime = myProps[z].keyTime(myKeys[1]);
                    if (newTime > oldTime) {
                        rd_Scooter_shiftKeyToNewTime(myProps[z], myKeys[1], (newTime - oldTime), (myKeys[1]));
                    } else {
                        rd_Scooter_shiftKeyToNewTime(myProps[z], myKeys[1], (newTime - oldTime), (myKeys[1] + 1));
                    }

                }
            }
        }
    }

    function applySpecialOperation(selectedLayers, data) {
        if (data.length === 1) {
            for (var j = 0; j < selectedLayers.length; j++) {
                var currentProps = selectedLayers[j].selectedProperties;
                if (currentProps.length === 0) {
                    alert("Choose the properties or set the property name");
                } else {
                    var ourData, propTimes, propVals;

                    for (var m = 0; m < currentProps.length; m++) {
                        if (currentProps[m].value.length > 1) {
                            ourData = extractData(data);
                            propTimes = ourData[3];
                            propVals = [];
                            for (var propTimeIndex = 0; propTimeIndex < propTimes.length; propTimeIndex++) {
                                var values = []
                                for (var i = 0; i < currentProps[m].value.length; i++) {
                                    values.push(ourData[i][propTimeIndex])
                                }
                                propVals.push(values);
                            }
                            currentProps[m].setValuesAtTimes(propTimes, propVals);
                        } else if (currentProps[m].value.length) {
                            //simple property
                            ourData = extractData(data);
                            propTimes = ourData[3];
                            propVals = ourData[0];
                        }
                        currentProps[m].setValuesAtTimes(propTimes, propVals);
                    }
                }
            }
        } else {
            //selecting special properties within selected layers
            for (var s = 0; s < selectedLayers.length; s++) {
                var curLayer = selectedLayers[s];
                var curProp, newData, propTimes, propVals;

                if (/[PAS]/.test(data[0])) {
                    var transformGroup = curLayer.property("ADBE Transform Group");
                    curProp = data[0] === "P"
                        ? transformGroup.property("ADBE Position")
                        : data[0] === "S"
                        ? transformGroup.property("ADBE Scale")
                        : transformGroup.property("ADBE Anchor Point");
                    newData = extractData(data[1]);
                    propTimes = newData[3];
                    propVals = [];
                    for (var i = 0; i < propTimes.length; i++) {
                        propVals.push([newData[0][i], newData[1][i], newData[2][i]]);
                    }
                    curProp.setValuesAtTimes(propTimes, propVals);
                } else if (/[OR]/.test(data[0])) {
                    curProp = curLayer.property("ADBE Transform Group").property("ADBE Opacity");
                    newData = extractData(data[1]);
                    propTimes = newData[3];
                    propVals = newData[0];
                    curProp.setValuesAtTimes(propTimes, propVals);
                } else if (data[0] === "E") {
                    var FI = parseInt(data[0].slice(1, 2));
                    var SI = parseInt(data[0].slice(2));
                    var ourEffAttr = curLayer.property("ADBE Effect Parade")(FI)(SI);
                    //now analyse and apply new values
                    if (ourEffAttr.value.length > 1) {//2-dimentinal property
                        newData = extractData(data[1]);
                        propTimes = newData[3];
                        propVals = [];
                        for (var o = 0; o < propTimes.length; o++) {
                            var values = [];
                            for (var i = 0; i < ourEffAttr.value.length; i++) {
                                values.push(newData[i][o])
                            }
                            propVals.push(values);
                        }
                        ourEffAttr.setValuesAtTimes(propTimes, propVals);
                    } else if (ourEffAttr.value.length) {
                        newData = extractData(data[1]);
                        propTimes = newData[3];
                        propVals = newData[0];
                        ourEffAttr.setValuesAtTimes(propTimes, propVals);
                    }
                } else if (data[0] === "M") {
                    var FI = parseInt(data[0].slice(1, 2));
                    var SI = parseInt(data[0].slice(2, 3));
                    var ourMaskAttr = curLayer.property("ADBE Mask Parade")(FI)(SI);
                    //now analyse and apply new values
                    if (ourMaskAttr.value.length === 2) {//2-dimentinal property
                        newData = extractData(data[1]);
                        propTimes = newData[3];
                        propVals = [];
                        for (var o = 0; o < propTimes.length; o++) {
                            var x = newData[0][o];
                            var y = newData[1][o];
                            propVals.push([x, y]);
                        }
                        ourMaskAttr.setValuesAtTimes(propTimes, propVals);
                    } else {
                        //simple property
                        newData = extractData(data[1]);
                        propTimes = newData[3];
                        propVals = newData[0];
                    }
                    ourMaskAttr.setValuesAtTimes(propTimes, propVals);
                } else {
                    alert("The property is undefined")
                }
            }
        }
    }
}